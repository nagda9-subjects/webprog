<pre><?php
include('../../db/credentials.php');

$conn = new PDO("mysql:host=localhost;dbname=$database", $username, $password);

$statement = $conn->prepare('SELECT * FROM teszt;');
$statement->execute();

// Fetch row by row:
while($row = $statement->fetch()) {
    echo "--------------------------------------------------------------------------\n";
    var_dump($row);
}

echo "\n\n==========================================================================\n\n";

// (Execute again and) Fetch all as "BOTH"
$statement->execute();
$rows_both  = $statement->fetchAll(PDO::FETCH_BOTH);
var_dump($rows_both);

echo "\n\n==========================================================================\n\n";

// (Execute again and) Fetch all as "ASSOC"
$statement->execute();
$rows_assoc  = $statement->fetchAll(PDO::FETCH_ASSOC);
var_dump($rows_assoc);

echo "\n\n==========================================================================\n\n";

// (Execute again and) Fetch all as "BOTH"
$statement->execute();
$rows_obj  = $statement->fetchAll(PDO::FETCH_OBJ);
var_dump($rows_obj);

?></pre>