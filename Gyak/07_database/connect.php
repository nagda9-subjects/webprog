<?php
include('../../db/credentials.php');

try {
    $conn = new PDO("mysql:host=localhost;dbname=$database", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    echo "Succesfully connected!";
}
catch(PDOException $e) {
    die("Connection failed: " . $e->getMessage());
}
