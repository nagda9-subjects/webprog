<pre><?php
include('../../db/credentials.php');

$conn = new PDO("mysql:host=localhost;dbname=$database", $username, $password);

$name = isset($_GET['name']) ? $_GET['name'] : "";

$statement = $conn->prepare('INSERT INTO teszt (name, created) VALUES (:name, :created)');
$statement->bindParam(':name', $name);
$statement->bindParam(':created', date('Y-m-d H:i:s'));
$statement->execute();

// Print some debug info
$statement->debugDumpParams();