<?php

interface Printable {
    function printMe();
}

class Person  {
    protected $name;

    // constructor
    function __construct($name) {
        $this->name = $name;
    }

    function getName() {
        return $this->name;
    }
}

class Student extends Person implements Printable {
    protected $university = 'PPKE';
    protected $faculty;

    // constructor
    function __construct($name, $faculty, $university=null) {
        // call parent constructor
        parent::__construct($name);

        // set properties
        $this->name = $name;
        $this->faculty = $faculty;
        if(!is_null($university)) {
            $this->university = $university;
        }
    }

    public function getUniversity() {
        return $this->university;
    }

    // built-in method for object to string conversion
    public function __toString() {
        return $this->name.' ('.$this->university.'-'.$this->faculty.')';
    }

    // interface
    public function printMe() {
        echo $this;
    }
}

$student = new Student('Gipsz Jakab', 'ITK');
$student->printMe(); // Gipsz Jakab (PPKE-ITK)
