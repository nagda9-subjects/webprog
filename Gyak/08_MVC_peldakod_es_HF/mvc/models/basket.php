<?php

require_once '../models/db.php';
require_once '../models/product.php';

class Basket {

    /** @var array<int,int> */
    protected $counter;

    /** @var PDO  */
    private $conn;

    /**
     * Basket constructor.
     * @param $session
     */
    public function __construct(& $session) {
        $db = DB::getInstance();
        $this->conn = $db->getConnection();

        $this->counter = & $session;
    }

    /**
     * Add a Product to the basket
     * @param Product $product
     */
    public function add($product) {
        // TODO: Kérdezd le a paraméterül kapott termék ID-ját, majd tedd be azt a kosárba.
        //  Figyelj rá, hogy a kosár a $this->counter mezőben van (ami egy referencia a $_SESSION['basket']-ra
    }

    /**
     * GETTERS
     */

    public function getTotal() {
        // TODO: Ez a metódus adja vissza a kosár összegét: SUM(termék darabszáma × termék ára)
    }

    public function getContent() {
        // TODO: ez a metódus adjon visssza egy olyan struktúrát, amiből a View-ben előállítható a kosár tartalma.
        //  Tehát valahogy (pl. egy tömbben) vissza kell adni a kosárban lévő termékeket, és azt, hogy az adott
        //  termékből mennyi van a kosárban. Figyelj rá, hogy a View lehetőleg csak formázást végezzen, tehát pl.
        //  darabszám × egységár szorzást ne (azt itt végezd el).
    }
}