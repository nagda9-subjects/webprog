<?php

require_once '../models/db.php';

class OrderForm {
    /** @var string  */
    protected $field_name;
    /** @var string  */
    protected $field_mail;
    /** @var string  */
    protected $field_comment;
    /** @var string  */
    protected $field_newsletter;
    /** @var string  */
    protected $field_terms;

    /** @var string|null  */
    protected $error_name = null;
    /** @var string|null  */
    protected $error_mail = null;
    /** @var string|null  */
    protected $error_comment = null;
    /** @var string|null  */
    protected $error_newsletter = null;
    /** @var string|null  */
    protected $error_terms = null;

    /** @var bool  */
    private $is_submitted = false;
    /** @var bool  */
    private $is_valid = false;

    private $conn;

    public function __construct($POST_DATA = null) {
        // Initialize DB connection
        $db = DB::getInstance();
        $this->conn = $db->getConnection();

        if(!is_null($POST_DATA)) {
            $this->field_name = $POST_DATA['name'] ?? "";
            $this->field_mail = $POST_DATA['mail'] ?? "";
            $this->field_comment = $POST_DATA['comment'] ?? "";
            $this->field_newsletter = $POST_DATA['newsletter'] ?? null;
            $this->field_terms = $POST_DATA['terms'] ?? "no";

            $this->is_submitted = isset($POST_DATA['submit']);
        }
    }

    public function validate() {
        // Validate name
        $this->valid = true;

        $this->error_name       = $this->validateName()       ? null : 'A név megadása kötelező!';
        $this->error_mail       = $this->validateMail()       ? null : 'Az email cím megadása kötelező!';
        $this->error_comment    = $this->validateComment()    ? null : 'A megjegyzés mező kitöltése hibás!';
        $this->error_newsletter = $this->validateNewsletter() ? null : 'A hírlevél mező kitöltése hibás!';
        $this->error_terms      = $this->validateTerms()      ? null : 'Az ÁSZF elfogadása kötelező!';

        $this->is_valid = is_null($this->error_name) && is_null($this->error_mail) && is_null($this->error_comment) &&
            is_null($this->error_newsletter) && is_null($this->error_terms);
    }

    // TODO: implementálandók a validate() metódusban hivatkozott validateMező() eljárások. A visszatérési értékük
    //  a validálás sikerétől függően igaz vagy hamis. Például:

    /**
     * @return bool
     */
    private function validateName() {
        return strlen($this->field_name) > 0;
    }

    /**
     * @param Basket $basket
     * @return bool Success
     */
    public function save($basket) {
        // TODO: implementáld a mentés műveletet. A mentés csak akkor sikerüljön, ha a form valid.
        //  Ha nem valid vagy SQL hiba történt, akkor hamissal kell visszatérni, egyébként ha a mentés sikeres, akkor
        //  legyen igaz visszatérési érték
    }

    /**
     * GETTERS
     */

    public function isSubmitted() {
        return $this->is_submitted;
    }

    public function isValid() {
        return $this->is_valid;
    }

    public function getNameValue() {
        return $this->field_name;
    }

    public function getNameError() {
        return $this->error_name;
    }

    public function getMailValue() {
        return $this->field_mail;
    }

    public function getMailError() {
        return $this->error_mail;
    }

    public function getCommentValue() {
        return $this->field_comment;
    }

    public function getCommentError() {
        return $this->error_comment;
    }

    public function getNewsletterValue() {
        return $this->field_newsletter == 'on' ? 'on' : 'off';
    }

    public function getNewsletterError() {
        return $this->error_newsletter;
    }

    public function getTermsValue() {
        return $this->field_terms == 'yes' ? 'yes' : 'no';
    }

    public function getTermsError() {
        return $this->error_terms;
    }
}