<?php
/** This view is for displaying the basket
 * Used variables:
 * @var Basket $BASKET
 */
?>
<table class="table table-striped">
    <thead>
    <tr>
        <th class="name">Termék</th>
        <th class="quantity">Mennyiség</th>
        <th class="price">Ár</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach($BASKET->getContent() as $item) {
        include('../views/basket_row.php');
    }
    ?>
    </tbody>
    <tfoot>
    <tr>
        <th class="name table-info" colspan="2">Összesen:</th>
        <th class="price table-info"><?php echo number_format($BASKET->getTotal(), 0, ',', ' ') ?> Ft</th>
    </tr>
    </tfoot>
</table>