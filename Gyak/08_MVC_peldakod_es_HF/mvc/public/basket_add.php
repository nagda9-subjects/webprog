<?php

require_once '../models/product.php';
require_once '../models/basket.php';

// Process user input
if(!isset($_GET['prod'])) {
    // redirect to main page
    header('Location: index.php');
}

// Get product id
$id = intval($_GET['prod']);
$product = Product::find($id);

if(is_null($product)) {
    // redirect to main page
    header('Location: index.php');
}

// Start session
session_start();
if(!isset($_SESSION['basket'])) {
    $_SESSION['basket'] = [];
}

// Create the basket
$BASKET = new Basket($_SESSION['basket']);
$BASKET->add($product);

// Redirect to the order page
header('Location: order.php');
