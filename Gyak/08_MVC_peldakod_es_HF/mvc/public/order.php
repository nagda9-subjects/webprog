<?php
require_once '../models/basket.php';
require_once '../models/orderform.php';

// Set the name of the current page
$PAGE = 'order';

// Start session
session_start();
if(!isset($_SESSION['basket'])) {
    $_SESSION['basket'] = [];
}

// Get the basket
$BASKET = new Basket($_SESSION['basket']);
$FORM   = new OrderForm($_POST);

// Check if the form is submitted
if($FORM->isSubmitted()) {

    // Try to validate
    $FORM->validate();

    // If the form is valid, save it!
    if($FORM->isValid()) {
        $SUCCESS = $FORM->save($BASKET);
    }
}


// Do the rendering of the page
include '../views/header.php';      // This requres the $PAGE variable
include '../views/order.php';       // This requires $PROMO_PRODUCTS and $OTHER_PRODUCTS
include '../views/footer.php';      // This has no variable requirements
