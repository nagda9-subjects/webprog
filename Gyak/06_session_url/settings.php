<?php
// We shall increase the number of cats in this session:

// Include the script responsible for session handling
include 'url_session.php';

// Start the url session engine
url_session_start();

// Get the lang attribute of the form:
if(isset($_POST['lang'])) {
    // validate:
    if(in_array($_POST['lang'], ['hu', 'en', 'de', 'es', 'fr'])) {
        // Set the language:
        $_URL_SESSION['lang'] = $_POST['lang'];
    }
}

// Redirect.
// IMPORTANT! Do NOT discard the session ID (token) from the URL or else the cats get lost :(

header('Location: index.php?token='.$_URL_TOKEN);