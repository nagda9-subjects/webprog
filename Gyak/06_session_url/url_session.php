<?php
// This PHP script contains a number of function declarations that can be used for URL session handling

// Global variables for the session:
$_URL_SESSION = null;
$_URL_TOKEN = null;

function url_session_start() {
    global $_URL_SESSION, $_URL_TOKEN;

    // Load the sessions.json file (this file stores the token-settings pairs)
    $tokens = json_decode(file_get_contents('sessions.json'), TRUE);

    print_r($tokens);

    // Let's initialize a variable; we'll use it for token generation
    $token_valid = false;

    // Check the URL of the current request
    if(isset($_GET['token'])) {
        // This means that there is a token in the URL. Let's find it in the list of tokens:
        if(array_key_exists($_GET['token'], $tokens)) {
            // The token exists. Check timeout:
            if(time() < $tokens[$_GET['token']]['valid-until']) {
                // The token is valid. Now we can set the values of the global variables:
                $_URL_TOKEN   = $_GET['token'];
                $_URL_SESSION = $tokens[$_GET['token']]['data'];
                // Also the token is valid, so we keep it:
                $token_valid = true;
            }
        }
    }

    // If the token is invalid (there is no token associated with the request, OR the token is an old one)
    if(!$token_valid) {
        // Generate a random (and non-existing) token:
        $token_good = false;
        while(!$token_good) {
            $token = md5(rand());
            $token_good = !array_key_exists($token, $tokens);
        }
        // Let's share the new token in the global variable.
        $_URL_TOKEN = $token;
    }

    // This registers a shutdown function so when the script finishes the url_session_save function executes
    register_shutdown_function('url_session_save');

    // done :)
}

function url_session_save() {
    global $_URL_SESSION, $_URL_TOKEN;

    // This function is to save the updated session data

    // Load the sessions.json file (this file stores the token-settings pairs)
    $tokens = json_decode(file_get_contents(__DIR__.'/sessions.json'), TRUE);

    // Let's get the current token value:
    $token = $_URL_TOKEN;

    // Let's check if there is already an entry for this token:
    if(!array_key_exists($token, $tokens)) {
        // if there is no such entry, create one!
        $tokens[$token] = [];
    }

    // update the timeout:
    $tokens[$token]['valid-until'] = time() + 5*60; // current time + 5*60 seconds (5 minutes);
    $tokens[$token]['data'] = $_URL_SESSION;

    // Write the database file:
    file_put_contents(__DIR__.'/sessions.json', json_encode($tokens));

    // done :)
}