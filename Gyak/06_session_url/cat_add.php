<?php
// We shall increase the number of cats in this session:

// Include the script responsible for session handling
include 'url_session.php';

// Start the url session engine
url_session_start();

// Get the actual number of cats:
if(!isset($_URL_SESSION['cats'])) {
    $_URL_SESSION['cats'] = 0;
}

// Increment:
$_URL_SESSION['cats']++;

// Redirect.
// IMPORTANT! Do NOT discard the session ID (token) from the URL or else the cats get lost :(
header('Location: index.php?token='.$_URL_TOKEN);