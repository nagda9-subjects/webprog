<?php
// Include the script responsible for session handling
include 'url_session.php';

// Start the url session engine
url_session_start();
?>

<h1>Session test</h1>
<h2>Some info about the current session</h2>
<pre>
$_URL_TOKEN   = "<?php print($_URL_TOKEN); ?>";
</pre>
<pre>
$_URL_SESSION = <?php var_dump($_URL_SESSION); ?>
</pre>

<h2>Some tools to change the settings</h2>
<a href="index.php?token=<?=$_URL_TOKEN?>">Reload this page AND PRESERVE THE SESSION ID</a>
<hr>
<a href="cat_add.php?token=<?=$_URL_TOKEN?>">This link increments the number of cats in the current session (if there are no cats it creates one).</a>
<hr>
<p>This form let's you modify the value of the 'lang' setting</p>
<form action="settings.php?token=<?=$_URL_TOKEN?>" method="post">
    <label for="lang">Language:</label>
    <select id="lang" name="lang">
        <option value="hu">magyar</option>
        <option value="en">English</option>
        <option value="de">Deutsch</option>
        <option value="es">Español</option>
        <option value="fr">Français</option>
    </select>
    <input type="submit">
</form>
