<pre>
<?php

$password = "Rumpelstiltskin";
$hash = password_hash($password, PASSWORD_BCRYPT, ['cost' => 12]);

echo $hash . PHP_EOL;

$valid = password_verify('Rumpelstiltskin', $hash);

if ($valid) {
    echo 'Password is valid!';
} else {
    echo 'Invalid password.';
}

?></pre>