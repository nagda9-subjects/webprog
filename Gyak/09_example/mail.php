<pre>
<?php

// Load PHPMailer
require '/opt/phpmailer/src/autoload.php';
/** @var PHPMailer\PHPMailer\PHPMailer $mail  */

try {
    $mail->addAddress('cimzett@example.com');
    $mail->Subject = 'Subject';

    $mail->isHTML(true); // Set email format to HTML
    $mail->Body    = 'This is the HTML message body <b>in bold!</b>';
    $mail->AltBody = 'This is the body in plain text for non-HTML mail clients';

    $mail->send();
    echo 'Message has been sent';

} catch (Exception $e) {
    echo "Message could not be sent. Mailer Error: {$mail->ErrorInfo}";
}


?></pre>
