<h2>A mod_rewrite és a jelen PHP router ilyen darabokra szedte az URL-t:</h2>
<pre><?php

$route = $_GET['route'] ?? 'home';
$route = explode('/', $route);

var_dump($route);

$__SERVER_ROOT = '/home/naszy';
$__CLIENT_ROOT = '/webprog/~naszy';

if($route[0] == 'home') {
    // require_once($__SERVER_ROOT.'/controllers/main.php');
} else if($route[0] == 'terms') {
    // require_once($__SERVER_ROOT.'/controllers/terms.php');
} else {
    // require_once($__SERVER_ROOT.'/controllers/404.php');
}

?></pre>

<h2>Továbbá néhány példa escape-elésre:</h2>
<?php
$string = "<b>Bold vagyok</b>, haha";
?>

<p>Csak úgy kiprintelve: <?php echo $string;                ?></p>
<p>htmlentities:         <?php echo htmlentities($string);  ?></p>
<p>strip_tags:           <?php echo strip_tags($string);    ?></p>
